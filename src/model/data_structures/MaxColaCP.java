package model.data_structures;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.CompareGenerator;

public class MaxColaCP <T extends Comparable<T>>  {

	private int numElementos;
	private Nodo<T> maxElemento;



	public MaxColaCP(){
		numElementos= 0;
		maxElemento= null;
	}


	public int darNumElementos(){
		return numElementos; 
	}

	public void agregar(T elemento){
		numElementos++;
		T actual =maxElemento.obtenerDato();
		if(maxElemento==null)
			maxElemento= new Nodo<T>(elemento, null) ;
		else if(numElementos>=1){
			if(elemento.compareTo(actual)==1) {
				actual=(T) maxElemento.darSiguiente();
				elemento = maxElemento.obtenerDato();
			}
			else {
				elemento = maxElemento.obtenerDato();
			}

		}
	}
	public T sacarMax() {
		if(maxElemento.obtenerDato()!=null) {
			return maxElemento.obtenerDato();
		}
		else {
			return null;
		}
	}

	public boolean esVacia() {
		if(maxElemento.obtenerDato()!=null) {
			return true;
		}
		else {
			return false;
		}
	}
}

