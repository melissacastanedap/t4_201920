package model.data_structures;

import static org.junit.Assert.assertNotNull;

public class MaxHeapCP  <T extends Comparable<T>>  {
	private int numElementos;
	private Nodo<T> maxElemento;



	public MaxHeapCP(){
		numElementos= 0;
		maxElemento= null;
	}
	
	
	public int darNumElementos(){
		return numElementos; 
	}
	
	public void agregar(T elemento){
		numElementos++;
		if(maxElemento==null)
			maxElemento= new Nodo<T>(elemento, null) ;
		else if(numElementos==1){
		
		}
	}
	
	public T sacarMax() {
		if(maxElemento.obtenerDato()!=null) {
		return maxElemento.obtenerDato();
		}
		else {
			return null;
		}
	}
	
	public boolean esVacia() {
		if(maxElemento.obtenerDato()!=null) {
			return true;
		}
		else {
			return false;
		}
	}
}
