package model.logic;

public class TravelTime implements Comparable<TravelTime>{

	private int trimestre;
	private int sourceId;
	private int distid;
	private int hod;
	private double mean_travel_time;
	private double standard_deviation_travel_time;
	
	public TravelTime(int pTrimestre, int pSourceId, int pDistid, int pHod, double pMeanTravelTime, double pstandardDeviationTravelTime)
	{
		trimestre=pTrimestre;
		sourceId= pSourceId;
		distid= pDistid;
		hod = pHod;
		mean_travel_time=pMeanTravelTime;
		standard_deviation_travel_time = pstandardDeviationTravelTime;

	}

	public  double darmeanTravelTime() 
	{  
		return mean_travel_time;
	}
	@Override
	public int compareTo(TravelTime o) {
		// TODO Auto-generated method stub
		int aux =0;
		
		if (o.darmeanTravelTime() > mean_travel_time)
		{
			aux = -1 ;
		}
		else if (o.darmeanTravelTime() < mean_travel_time)
		{
			aux =1;
		}
		else if (o.darmeanTravelTime() == mean_travel_time)
		{
			aux=0;
				
		}
		
		return aux;

	}
}
