package view;

import model.logic.MVCModelo;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("1. cargar datos");
			System.out.println("2. Consultar viajes para una hora dada");
			System.out.println("3. Ordenar con Shell Sort");
			System.out.println("4. Ordenar con Merge Sort");
			System.out.println("5. ");
			System.out.println("6. Medir la eficiencia de los 3 algoritmos");
			System.out.println("7. Generar Muestra");
			System.out.println("8. N tiempos promedio de viajes mas largos");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			System.out.println(modelo);		
		}
		
		
		
		
		
		
}
